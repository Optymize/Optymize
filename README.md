# LaTeX-Vorlage für Fortgeschrittenenpraktikum an der TU Darmstadt

Die Vorlage in diesem Projekt wurde für die Verwendung im Fortgeschrittenenpraktikum der TU Darmstadt entworfen. Sie basiert auf [den offiziellen Vorlagen der Universität](https://github.com/tudace/tuda_latex_templates/blob/master/README.md).

## Benutzung

Vor der Benutzung dieser Vorlage muss LaTeX, inklusive des Paketes der TU Darmstadt (`tuda-ci`), installiert werden. Mehr Informationen dazu befinden sich [auf der Webseite des Zentrums für Computational Engineering der der TU Darmstadt](https://www.ce.tu-darmstadt.de/ce/latex_tuda/index.de.jsp). Im speziellen muss [das Logo der TU Darmstadt](https://download.hrz.tu-darmstadt.de/protected/CE/TUDa_LaTeX/tuda_logo.pdf) heruntergeladen und in den Projekt-Ordner eingefügt werden.

Anschließend kann das Projekt einfach in einem LaTeX-Editor geöffnet werden oder auf der Kommandozeile gebaut werden:
```bash
pdflatex essay.tex && biber essay && pdflatex essay.tex
```

Mithilfe von GitLab CI wird das Dokument automatisch gebaut und kann dann (nach individueller Anpassung) von folgender Adresse heruntergeladen werden:
```
https://git.rwth-aachen.de/nicos-ninjas/templates/template-fp/-/jobs/artifacts/master/raw/essay.pdf?job=build
```

Bevor dies funktioniert, muss jedoch erst das Logo der TU Darmstadt in das Repository eingefügt und eventuell GitLab Runner für die CI aktiviert werden.

## Lizenz

Der Inhalt dieses Projekts steht unter der LaTeX Project Public License (LPPL) Version 1.3c. Mehr Informationen befinden sich in [LICENSE.md](LICENSE.md).
